package com.unab.camerica;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ListView;

import com.google.gson.Gson;
import com.google.gson.internal.LinkedTreeMap;
import com.unab.camerica.adapters.PartidoAdapter;
import com.unab.camerica.model.Equipo;
import com.unab.camerica.model.Partido;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setTheme(R.style.AppTheme);
        setContentView(R.layout.activity_main);

        PartidoAdapter adapter = new PartidoAdapter(this, cargarPartidos());
        ListView lista = findViewById(R.id.partidos);

        View header = getLayoutInflater().inflate(R.layout.partidos_header, null);

        lista.setAdapter(adapter);
        lista.addHeaderView(header);
    }

    private Partido[] cargarPartidos() {
        SharedPreferences preferences = getSharedPreferences("preferencias", Context.MODE_PRIVATE);
        Gson gson = new Gson();

        String stringPartidos = preferences.getString("partidos", "");
        String stringPaises = preferences.getString("paises", "");

        ArrayList<LinkedTreeMap<String, Object>> listaPartidos = gson.fromJson(stringPartidos, ArrayList.class);
        ArrayList<LinkedTreeMap<String, Object>> listaPaises = gson.fromJson(stringPaises, ArrayList.class);

        ArrayList<Partido> partidos = new ArrayList<>();
        for(LinkedTreeMap<String, Object> partidoActual: listaPartidos) {
            int localID = ((Double)partidoActual.get("local")).intValue();
            int visitaID = ((Double)partidoActual.get("visita")).intValue();
            LinkedTreeMap<String, Object> dataLocal = listaPaises.get(localID);
            LinkedTreeMap<String, Object> dataVisita = listaPaises.get(visitaID);
            Equipo local = new Equipo(
                    dataLocal.get("api_id").toString(),
                    (String)dataLocal.get("bandera"),
                    (String)dataLocal.get("codigo"),
                    (String)dataLocal.get("nombre")
            );
            Equipo visita = new Equipo(
                    dataVisita.get("api_id").toString(),
                    (String)dataVisita.get("bandera"),
                    (String)dataVisita.get("codigo"),
                    (String)dataVisita.get("nombre")
            );
            Partido partido = new Partido(
                    local,
                    visita,
                    (String)partidoActual.get("hora"),
                    (String)partidoActual.get("fecha")
            );
            partidos.add(partido);
        }
        return partidos.toArray(new Partido[partidos.size()]);
    }

}
